
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenil
 */
public class MySQLConnect {
    /*
        * The below method created a connection with mysql database named as 'whatsapp' with the user account named
          'kenilshah' whose password is 'kenilshah'
        * It returns java.sql.Connection object if the connection was successfully established otherwise null.
    
    */
    private static final String DB_NAME = "whatsapp";
    private static final String CONNECTION_STRING = "jdbc:mysql://localhost:3306/" + DB_NAME;
    
    public static Connection getConnection() {
        try {
            Connection conn = DriverManager.getConnection(CONNECTION_STRING, "kenilshah", "kenilshah");
            return conn;
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Connection Failed " + e.getMessage());
            return null;
        }
        
    }
    
    
}
