import java.util.regex.Pattern;
import java.util.regex.Matcher;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenil
 */
public class JSON {
    public static String getUsernameToBeMessagedFromJSON(String jsonMessage){
        // String jsonRegex = "(^({)[\n\t]*(.*)(:)(.*)(,)[\n\t]*(.*)(:)(.*)[\n\t]*([};]*)$[\n\t]*)";
        String jsonPattern = "(([^\\s].*)(:)[\\s]*(\"([^\n]*))\")";
        Pattern pattern = Pattern.compile(jsonPattern);
        Matcher matcher = pattern.matcher(jsonMessage);
        int count = 0;
        while(matcher.find()){
                if((matcher.group(1)).contains("to")){
                        // System.out.println("User : " + matcher.group(4));
                        return matcher.group(5);
                }
        }
        return null;
    }
    public static String getUsernameFromJSON(String jsonMessage){
        // String jsonRegex = "(^({)[\n\t]*(.*)(:)(.*)(,)[\n\t]*(.*)(:)(.*)[\n\t]*([};]*)$[\n\t]*)";
        String jsonPattern = "(([^\\s].*)(:)[\\s]*(\"([^\n]*))\")";
        Pattern pattern = Pattern.compile(jsonPattern);
        Matcher matcher = pattern.matcher(jsonMessage);
        int count = 0;
        while(matcher.find()){
            if((matcher.group(1)).contains("from")){
                    // System.out.println("User : " + matcher.group(4));
                    return matcher.group(5);
            }
        }
        return null;
    }
    public static String getMessageFromJSON(String jsonMessage){
        // String jsonRegex = "(^({)[\n\t]*(.*)(:)(.*)(,)[\n\t]*(.*)(:)(.*)[\n\t]*([};]*)$[\n\t]*)";
        String jsonPattern = "(([^\\s].*)(:)[\\s]*(\"([^\n]*))\")";
        Pattern pattern = Pattern.compile(jsonPattern);
        Matcher matcher = pattern.matcher(jsonMessage);
        int count = 0;
        while(matcher.find()){
            if((matcher.group(1)).contains("message")){
                    // System.out.println("Message : " + matcher.group(4));
                    return matcher.group(5);
            }
        }
        return null;
    }
    public static String createJSONMessage(String username,String usernameToBeMessaged, String message){
        String json = "{" + "\n\t" + "\"from user\" : "+ "\"" + username + "\"" + "," + "\n\t" + "\"to user\" : " + "\"" + usernameToBeMessaged + "\"" + "," + "\n\t" + "\"message\" : " + "\"" + message + "\"" + "\n" + "};null";
        return json;
    }
}
