
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenil
 * 
 * Client Side Thread
 */
public class ReadMessagesFromServer extends Thread{
    private Socket socket;
    private Client client;
    private File usernameFile;
    private File usernameToBeMessagedFile;
    
//    private Boolean gate = false;
    public ReadMessagesFromServer(Socket socket, Client client){
        this.socket = socket;
        this.client = client;
    }
    public void run(){
        try {
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String response;
            String finalMessage = "";
            String message = "";
            String username="" ,usernameToBeMessaged="";
            while(true){
                while(true){
                    do{
                        response = input.readLine();
                        if(response != null){
                            finalMessage += response + "\n";
                        }
                    }while(!"};null".contains(response));
                    message = JSON.getMessageFromJSON(finalMessage);
                    if(message != null){
                        username = JSON.getUsernameFromJSON(finalMessage);
                        usernameToBeMessaged = JSON.getUsernameToBeMessagedFromJSON(finalMessage);
                        
                        insertMessagesIntoFile(username, usernameToBeMessaged,finalMessage);
                        
                        message = JSON.getMessageFromJSON(finalMessage);
                        showMessagesOnUI(username, message);
                        finalMessage = "";
                        break;
                    }
                    
                }
            }
            
        }catch(IOException e){
            e.printStackTrace();
        }
        
    }
    private void insertMessagesIntoFile(String username, String usernameToBeMessaged, String finalMessage) throws IOException{
        PrintStream writer1,writer2;
        usernameFile = new File("./src/resources/chats/"+ username + "-to-" + usernameToBeMessaged + ".json");
        usernameToBeMessagedFile = new File("./src/resources/chats/"+ usernameToBeMessaged + "-to-" + username + ".json");
        writer1  = new PrintStream(new FileOutputStream(usernameFile,true), true);
        writer2  = new PrintStream(new FileOutputStream(usernameToBeMessagedFile,true), true);
        writer1.println(finalMessage);
        writer2.println(finalMessage);
        writer1.close();
        writer2.close();
    }
    private void showMessagesOnUI(String username,  String message){
        GridBagConstraints c = new GridBagConstraints();
        JPanel pnlShowMessages = client.getPnlShowMessages(username);
        JLabel l = new JLabel(message, JLabel.LEFT);
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.add(l);
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.gridheight = 1;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.NORTHWEST;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.ipadx = 20;
        p.setBorder(new EmptyBorder(5,8,5,20));
        c.insets = new Insets(5,0,5,0);
        pnlShowMessages.add(p, c);
        pnlShowMessages.updateUI();
        c.weightx = 0.0;
        c.weighty = 0.0;
         
    }
}
