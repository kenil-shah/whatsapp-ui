
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import static java.awt.Font.PLAIN;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenil
 */
public class Client extends javax.swing.JFrame implements MouseListener{
    private CardLayout cardLayout, cardLayoutForChat;
    private Socket clientSocket;
    private BufferedReader input;
    private PrintWriter output;
    private String username;
    private String usernameToBeMessaged;
    private int dummyCount = 1;
    private JPanel pnlShowMessages = new JPanel();
    private Map<String, JPanel> loggedInUserMap = new HashMap<>();
    private Map<String , JPanel> pnlShowMessagesMap = new HashMap<>();
    private List<String> loggedInUsers = new ArrayList<>();
    private JPanel pnlCustomUser = new JPanel();
    private JPanel userPnl = new JPanel();
    private Color userPnlColor = new Color(42,60,70);
    private List<JLabel> clickedUserList = new ArrayList<>();
    private Map<String,ReadUserMessages> readUserMessagesMap = new HashMap<>();
    private Map<String,ReadMessagesFromServer> readMessagesFromServerMap = new HashMap<>();
    private Client client;
    private JTextField txtMessage = new JTextField();
    private Map<String, JTextField> txtMessageMap = new HashMap<>();
    /**
     * Creates new form Client
     */
    public Client() {
        initComponents();
        client = this;
        this.setResizable(false);
        pnlShowActiveUsers.setLayout(new GridLayout(5,0,0,3));
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        lblHeading = new javax.swing.JLabel();
        pnlMessages = new javax.swing.JPanel();
        pnlBg = new javax.swing.JPanel();
        pnlSignin = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        lblSignin = new javax.swing.JLabel();
        lblPassword = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnSubmit = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        lblUsername1 = new javax.swing.JLabel();
        txtPassword = new javax.swing.JPasswordField();
        txtUsername = new javax.swing.JTextField();
        pnlMainUI = new javax.swing.JPanel();
        pnlShowUsers = new javax.swing.JPanel();
        pnlHeader = new javax.swing.JPanel();
        pnlYourUsername = new javax.swing.JPanel();
        lblYourUsername = new javax.swing.JLabel();
        scrollPaneShowUsers = new javax.swing.JScrollPane();
        pnlShowActiveUsers = new javax.swing.JPanel();
        pnlTitle = new javax.swing.JPanel();
        lblTitle = new javax.swing.JLabel();
        pnlChat = new javax.swing.JPanel();
        pnlStartup = new javax.swing.JPanel();
        lblStartup = new javax.swing.JLabel();
        pnlChatUI = new javax.swing.JPanel();
        btnSend = new javax.swing.JButton();
        scrollPaneShowMessages = new javax.swing.JScrollPane();
        pnlTxtMessage = new javax.swing.JPanel();

        lblHeading.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        lblHeading.setText("Whatsapp");

        pnlMessages.setLayout(new java.awt.BorderLayout());

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("WhatsApp");

        pnlBg.setBackground(new java.awt.Color(26, 46, 76));
        pnlBg.setAlignmentX(Component.CENTER_ALIGNMENT);
        pnlBg.setLayout(new java.awt.CardLayout());

        pnlSignin.setBackground(new java.awt.Color(26, 46, 76));

        jPanel4.setBackground(new java.awt.Color(0, 153, 153));
        jPanel4.setAlignmentX(Component.CENTER_ALIGNMENT);
        jPanel4.setAlignmentY(Component.CENTER_ALIGNMENT);

        jPanel1.setOpaque(false);

        lblSignin.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        lblSignin.setForeground(new java.awt.Color(255, 255, 255));
        lblSignin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSignin.setText("WhatsApp");
        lblSignin.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblSignin.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        lblPassword.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        lblPassword.setForeground(new java.awt.Color(255, 255, 255));
        lblPassword.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPassword.setText("Enter Password ");

        jPanel2.setOpaque(false);

        btnSubmit.setText("Submit");
        btnSubmit.setAlignmentX(Component.CENTER_ALIGNMENT);
        btnSubmit.setPreferredSize(new java.awt.Dimension(150, 59));
        btnSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitActionPerformed(evt);
            }
        });
        jPanel2.add(btnSubmit);

        jPanel3.setBackground(new java.awt.Color(0, 255, 204));
        jPanel3.setOpaque(false);

        lblUsername1.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        lblUsername1.setForeground(new java.awt.Color(255, 255, 255));
        lblUsername1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblUsername1.setText("Enter Username ");

        txtPassword.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        txtPassword.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtPassword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPasswordKeyPressed(evt);
            }
        });

        txtUsername.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        txtUsername.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtUsername.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        txtUsername.setOpaque(true);
        txtUsername.setPreferredSize(new java.awt.Dimension(350, 40));
        txtUsername.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUsernameKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 878, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(lblPassword, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblSignin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblUsername1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(274, 274, 274))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(272, 272, 272))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(lblSignin, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addComponent(lblUsername1, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(81, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout pnlSigninLayout = new javax.swing.GroupLayout(pnlSignin);
        pnlSignin.setLayout(pnlSigninLayout);
        pnlSigninLayout.setHorizontalGroup(
            pnlSigninLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSigninLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlSigninLayout.setVerticalGroup(
            pnlSigninLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSigninLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlBg.add(pnlSignin, "card3");

        pnlMainUI.setBackground(new java.awt.Color(26, 46, 76));

        pnlHeader.setBackground(new java.awt.Color(255, 255, 255));

        pnlYourUsername.setBackground(new java.awt.Color(42, 60, 70));
        pnlYourUsername.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        pnlYourUsername.setForeground(new java.awt.Color(255, 255, 255));
        pnlYourUsername.setLayout(new java.awt.BorderLayout());

        lblYourUsername.setBackground(new java.awt.Color(255, 255, 255));
        lblYourUsername.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        lblYourUsername.setForeground(new java.awt.Color(255, 255, 255));
        lblYourUsername.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblYourUsername.setText("Your Username");
        lblYourUsername.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        pnlYourUsername.add(lblYourUsername, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout pnlHeaderLayout = new javax.swing.GroupLayout(pnlHeader);
        pnlHeader.setLayout(pnlHeaderLayout);
        pnlHeaderLayout.setHorizontalGroup(
            pnlHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlYourUsername, javax.swing.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
        );
        pnlHeaderLayout.setVerticalGroup(
            pnlHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlHeaderLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(pnlYourUsername, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        scrollPaneShowUsers.setBorder(null);
        scrollPaneShowUsers.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        pnlShowActiveUsers.setBackground(new java.awt.Color(42, 60, 70));
        pnlShowActiveUsers.setLayout(new java.awt.GridLayout(5, 0, 0, 1));
        scrollPaneShowUsers.setViewportView(pnlShowActiveUsers);

        javax.swing.GroupLayout pnlShowUsersLayout = new javax.swing.GroupLayout(pnlShowUsers);
        pnlShowUsers.setLayout(pnlShowUsersLayout);
        pnlShowUsersLayout.setHorizontalGroup(
            pnlShowUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlShowUsersLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(pnlShowUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(scrollPaneShowUsers)
                    .addComponent(pnlHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        pnlShowUsersLayout.setVerticalGroup(
            pnlShowUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlShowUsersLayout.createSequentialGroup()
                .addComponent(pnlHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(scrollPaneShowUsers, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
        );

        pnlTitle.setBackground(new java.awt.Color(242, 242, 244));
        pnlTitle.setLayout(new java.awt.BorderLayout());

        lblTitle.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        lblTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitle.setText("Start Chatting!!!!!!");
        pnlTitle.add(lblTitle, java.awt.BorderLayout.CENTER);

        pnlChat.setLayout(new java.awt.CardLayout());

        pnlStartup.setBackground(new java.awt.Color(242, 242, 244));
        pnlStartup.setLayout(new java.awt.BorderLayout());

        lblStartup.setBackground(new java.awt.Color(242, 242, 244));
        lblStartup.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        lblStartup.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblStartup.setIcon(new javax.swing.ImageIcon("/Users/kenil/Documents/programming/advjava/networking/Whatsapp-Clone/src/resources/msg-icon.png")); // NOI18N
        pnlStartup.add(lblStartup, java.awt.BorderLayout.CENTER);

        pnlChat.add(pnlStartup, "pnlStartup");

        pnlChatUI.setBackground(new java.awt.Color(42, 60, 70));

        btnSend.setBackground(new java.awt.Color(12, 245, 116));
        btnSend.setText("Send");
        btnSend.setBorderPainted(false);
        btnSend.setOpaque(true);
        btnSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendActionPerformed(evt);
            }
        });

        pnlTxtMessage.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout pnlChatUILayout = new javax.swing.GroupLayout(pnlChatUI);
        pnlChatUI.setLayout(pnlChatUILayout);
        pnlChatUILayout.setHorizontalGroup(
            pnlChatUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlChatUILayout.createSequentialGroup()
                .addComponent(pnlTxtMessage, javax.swing.GroupLayout.DEFAULT_SIZE, 508, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSend, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
            .addComponent(scrollPaneShowMessages)
        );
        pnlChatUILayout.setVerticalGroup(
            pnlChatUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlChatUILayout.createSequentialGroup()
                .addComponent(scrollPaneShowMessages)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlChatUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnSend, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                    .addComponent(pnlTxtMessage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(13, 13, 13))
        );

        pnlChat.add(pnlChatUI, "pnlChatUI");

        javax.swing.GroupLayout pnlMainUILayout = new javax.swing.GroupLayout(pnlMainUI);
        pnlMainUI.setLayout(pnlMainUILayout);
        pnlMainUILayout.setHorizontalGroup(
            pnlMainUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMainUILayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(pnlShowUsers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(pnlMainUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlChat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        pnlMainUILayout.setVerticalGroup(
            pnlMainUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMainUILayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(pnlMainUILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlShowUsers, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnlMainUILayout.createSequentialGroup()
                        .addComponent(pnlTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(pnlChat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(0, 0, 0))
        );

        pnlBg.add(pnlMainUI, "pnlMainUI");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBg, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /*EVENT HANDLERS START*/
    private void btnSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitActionPerformed
        // TODO add your handling code here:
        cardLayout = (CardLayout)pnlBg.getLayout();
        cardLayoutForChat = (CardLayout)pnlChat.getLayout();
        try{
            Socket socket = new Socket("localhost", 1106);
            this.clientSocket = socket;
            this.username = txtUsername.getText();
            input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            output = new PrintWriter(clientSocket.getOutputStream(),true);
            client.setResizable(true);
            while(true){
                if(registerUser(socket)){ 
                    lblYourUsername.setText(username);
                    cardLayout.show(pnlBg, "pnlMainUI");
                    cardLayoutForChat.show(pnlChat, "pnlStartup");
                    new ReadMessagesFromServer(this.clientSocket, this).start();
                    break;
                }
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Issue : " + e.getMessage());
        }
        
        
    }//GEN-LAST:event_btnSubmitActionPerformed
    @Override 
    public void mouseClicked(MouseEvent e){
        cardLayoutForChat.show(pnlChat, "pnlChatUI");
        usernameToBeMessaged = e.getComponent().getName();
//        System.out.println(usernameToBeMessaged);
        if(!addUserToList(usernameToBeMessaged)){
            lblTitle.setText(usernameToBeMessaged);
            scrollPaneShowMessages.setViewportView(this.pnlShowMessagesMap.get(usernameToBeMessaged));
            scrollPaneShowMessages.updateUI();
            pnlTxtMessage.removeAll();
            txtMessage = this.txtMessageMap.get(usernameToBeMessaged);
            pnlTxtMessage.add(txtMessage);
            txtMessage.requestFocus();
            
        }
        if(!clickedUserList.contains(e.getComponent())){
            new FetchPreviousMessages(this).start();
            clickedUserList.add((JLabel)e.getComponent());
            ReadUserMessages readUserMessages = new ReadUserMessages(this.clientSocket,this);
            readUserMessages.start();
            readUserMessagesMap.put(usernameToBeMessaged,readUserMessages);
//            System.out.println(e.getComponent());
        }
    }
    @Override
    public void mousePressed(MouseEvent e){}
    @Override
    public void mouseEntered(MouseEvent e){
//        System.out.println(e.getComponent());
        e.getComponent().setBackground(new Color(42,60,70,100));
//        e.getComponent().setForeground(Color.BLACK);   
    }
    @Override
    public void mouseReleased(MouseEvent e){}
    @Override
    public void mouseExited(MouseEvent e){
        e.getComponent().setBackground(userPnlColor);
    }

    private void btnSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendActionPerformed
        // TODO add your handling code here:
        String currUsername = lblTitle.getText();
//        System.out.println(readUserMessagesMap.get(currUsername));
        try {
            readUserMessagesMap.get(currUsername).sleep(100);
        }catch(InterruptedException e){}
        readUserMessagesMap.get(currUsername).messageComplete = true;
//        txtMessage.setText("");
    }//GEN-LAST:event_btnSendActionPerformed

    private void txtUsernameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUsernameKeyPressed
        // TODO add your handling code here:

        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            if(txtUsername.getText().equals("")){
                JOptionPane.showMessageDialog(this, "Enter a Username!!");
                return;
            }
            txtPassword.requestFocus();
        }
    }//GEN-LAST:event_txtUsernameKeyPressed

    private void txtPasswordKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPasswordKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            if(txtPassword.getText().equals("")){
                JOptionPane.showMessageDialog(this, "Enter a Password!!");
                return;
            }
            btnSubmitActionPerformed(null);
        }
    }//GEN-LAST:event_txtPasswordKeyPressed
    
    
        /*EVENT HANDLERS END*/
    
    /*CUSTOM METHODS START*/
    public void displayLoggedInUserMap(){
        for(String key : loggedInUserMap.keySet()){
            System.out.println("Hashmap : ");
            System.out.println(key + " : " +  loggedInUserMap.get(key));
        }
    }
    public void removeUser(String username){
        System.out.println("Username removed : " + username);
//        System.out.println(loggedInUserMap.get(username));     
//          displayLoggedInUserMap(); 
        try {
            Thread.sleep(300);
        }catch(InterruptedException e){}
        this.pnlShowActiveUsers.remove(loggedInUserMap.get(username));
        this.pnlShowActiveUsers.updateUI();
        loggedInUserMap.remove(username);
        loggedInUsers.remove(username);
    }
    
    public void addNewUser(int rows,String username){
        int count = 5;
        if(rows > count){
//            count = rows;
            ((GridLayout)pnlShowActiveUsers.getLayout()).setRows(rows);
        }
//        pnlShowActiveUsers.setLayout(new GridLayout(5,0,0,1));
        
        this.pnlCustomUser = createUserPanel(username);
        pnlShowActiveUsers.add(pnlCustomUser);
        pnlShowActiveUsers.updateUI();
        loggedInUserMap.put(username, pnlCustomUser); 
        
            
        addUserToList(username);
        
        pnlShowMessages = new JPanel();
        pnlShowMessages.setBackground(new Color(73,93,99));
        pnlShowMessages.setLayout(new GridBagLayout());
        this.pnlShowMessagesMap.put(username, pnlShowMessages);
        
        txtMessage = createTxtMessage();
        addTxtMessageEvents();
        pnlTxtMessage.add(txtMessage, BorderLayout.CENTER);
        this.txtMessageMap.put(username, txtMessage);
        
    }
    public JTextField createTxtMessage(){
        JTextField txtMessage1 = new JTextField();
        txtMessage1.setText("Type a Message....");
        txtMessage1.setFont(new Font("Lucida Grande", PLAIN, 14));
        txtMessage1.setForeground(new Color(153,153,153));
        return txtMessage1;
    }
    public void addTxtMessageEvents(){
        txtMessage.addKeyListener(new KeyAdapter(){
            public void keyPressed(KeyEvent e){
                if(!txtMessage.getText().equals("")){
                    if(e.getKeyCode() == KeyEvent.VK_ENTER){
                        btnSendActionPerformed(null);
                    }
                }
            }
         });
        txtMessage.addFocusListener(new FocusAdapter(){
           public void focusGained(FocusEvent e){
                if(txtMessage.getText().equals("Type a Message....")){
                    txtMessage.setForeground(Color.BLACK);
                    txtMessage.setText("");
                }else {
                    txtMessage.setCaretPosition(txtMessage.getText().length());
                }
           } 
           public void focusLost(FocusEvent e){
                if(txtMessage.getText().matches(" *")){
                    txtMessage.setForeground(Color.LIGHT_GRAY);
                    txtMessage.setText("Type a Message....");
                }
           }
        });
    }
    public JPanel createUserPanel(String username){
        userPnl = new JPanel();
        userPnl.setBackground(userPnlColor);
        userPnl.setLayout(new BorderLayout());
        userPnl.setBorder(BorderFactory.createLineBorder(Color.WHITE));
        JLabel lbl = new JLabel(username, JLabel.CENTER);
        lbl.setFont(new Font("SansSerif", Font.PLAIN, 18));
        lbl.setName(username);
        lbl.addMouseListener(this);
        lbl.setForeground(Color.WHITE);
        lbl.setBackground(userPnlColor);
        lbl.setOpaque(true);
        userPnl.add(lbl);
        
        return userPnl;
    }
    
    private boolean addUserToList(String username){
        if(!loggedInUsers.contains(username)){
            loggedInUsers.add(username);
            return true;
        }
        return false;
    }
    
    private boolean registerUser(Socket socket) throws Exception{
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<Boolean> future = executor.submit(new CheckNewUser(this , socket));
        Boolean temp = future.get();
        executor.shutdown();
        return temp;
    }
    
    private void resetAll(){
        txtUsername.setText("");
        txtMessage.setText("");
    }

    public JTextField getTxtUsername(){
        return this.txtUsername;
    }
    public JTextField getTxtMessage(String username){
        return this.txtMessageMap.get(username);
    }
    public CardLayout getCardLayout(){
        return this.cardLayout;
    }
    public JPanel getPnlBg(){
        return this.pnlBg;
    }
    public String getUsername(){
        return this.username;
    }
    public String getUsernameToBeMessaged(){
        return this.usernameToBeMessaged;
    }
    
    public JLabel getLblYourUsername(){
        return this.lblYourUsername;
    }
    public JPanel getPnlShowMessages(String username){
        return this.pnlShowMessagesMap.get(username);
    }
    /*CUSTOM METHODS END*/
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Mac OS X".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Client().setVisible(true);
             
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSend;
    private javax.swing.JButton btnSubmit;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel lblHeading;
    private javax.swing.JLabel lblPassword;
    private javax.swing.JLabel lblSignin;
    private javax.swing.JLabel lblStartup;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JLabel lblUsername1;
    private javax.swing.JLabel lblYourUsername;
    private javax.swing.JPanel pnlBg;
    private javax.swing.JPanel pnlChat;
    private javax.swing.JPanel pnlChatUI;
    private javax.swing.JPanel pnlHeader;
    private javax.swing.JPanel pnlMainUI;
    private javax.swing.JPanel pnlMessages;
    private javax.swing.JPanel pnlShowActiveUsers;
    private javax.swing.JPanel pnlShowUsers;
    private javax.swing.JPanel pnlSignin;
    private javax.swing.JPanel pnlStartup;
    private javax.swing.JPanel pnlTitle;
    private javax.swing.JPanel pnlTxtMessage;
    private javax.swing.JPanel pnlYourUsername;
    private javax.swing.JScrollPane scrollPaneShowMessages;
    private javax.swing.JScrollPane scrollPaneShowUsers;
    private javax.swing.JPasswordField txtPassword;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables
}
