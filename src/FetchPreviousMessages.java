
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenil
 */
public class FetchPreviousMessages extends Thread{
    private Client client;
    private File file;
    private String clientUsername;
    private String clientUsernameToBeMessaged;
    public FetchPreviousMessages(Client client) {
        this.client = client;
        clientUsername = client.getUsername();
        clientUsernameToBeMessaged = client.getUsernameToBeMessaged();
    }
    @Override
    public void run() {
        
        String message = null;
        String username = null,usernameToBeMessaged = null;
        try {
            file = new File("./src/resources/chats/"+ clientUsername + "-to-" + clientUsernameToBeMessaged + ".json");
            if(file.exists()){
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                String line = "";
                while((line = br.readLine()) != null) {
                    System.out.println("Line :" + line);
                    if(username == null){
                        username = JSON.getUsernameFromJSON(line);
                        System.out.println("Username :" + username);
                    }
                    if(usernameToBeMessaged == null){
                        usernameToBeMessaged = JSON.getUsernameToBeMessagedFromJSON(line);
                        System.out.println("UsernameToBeMessaged :" + usernameToBeMessaged);
                    }
                    if(message == null){
                        message = JSON.getMessageFromJSON(line);
                        System.out.println("Message :" + message);
                    }
                    if(username != null && usernameToBeMessaged != null && message != null){
                        showMessagesOnUI(username, usernameToBeMessaged, message);
                        username = usernameToBeMessaged = message = null;
                    }
                    
                } 
            }
            
        }catch(IOException e) {
//            e.printStackTrace();
        }
        
    }
    private void showMessagesOnUI(String username,String usernameToBeMessaged, String message){
        GridBagConstraints c = new GridBagConstraints();
        JPanel pnlShowMessages = client.getPnlShowMessages(clientUsernameToBeMessaged);;
        JLabel l = new JLabel(message, JLabel.LEFT);
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.add(l);
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.gridheight = 1;
        c.fill = GridBagConstraints.NONE;
        if(username.equals(clientUsername)){
            c.anchor = GridBagConstraints.NORTHWEST;
            p.setBorder(new EmptyBorder(5,8,5,20));
        }else {
            c.anchor = GridBagConstraints.NORTHEAST;
            p.setBorder(new EmptyBorder(5,20,5,8));
        }
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.ipadx = 20;
        c.insets = new Insets(5,0,5,0);
        pnlShowMessages.add(p, c);
        pnlShowMessages.updateUI();
        c.weightx = 0.0;
        c.weighty = 0.0;
         
    }
}
