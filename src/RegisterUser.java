
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.locks.ReentrantLock;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenil
 */
// Server side Thread
public class RegisterUser extends Thread{
    private Socket clientSocket;
    private BufferedReader input;
    private PrintWriter output;
    private boolean configBool = false;
    private Socket configClientSocket;
    private PrintWriter configOutput;
    private ReentrantLock bufferLock;
//    private ServerSocket configServerSocket;
    public RegisterUser(Socket clientSocket, ReentrantLock bufferLock){
        this.clientSocket = clientSocket;
        this.bufferLock = bufferLock;
    }
    @Override
    public void run(){
        try{
            input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            output = new PrintWriter(clientSocket.getOutputStream(),true);
            String username;
            while(true){
                username = input.readLine();
                if(username != null && Server.addUser(username, clientSocket)){
                    output.println("valid");
                    Server.displayMap();
                    break;
                }else {
                    output.println("invalid");
                }
            }
            
            new SendMessages(clientSocket, username, bufferLock).start();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    

}
