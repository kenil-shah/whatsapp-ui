
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenil
 * 
 * Client Side Thread
 */

public class CheckNewUser implements Callable{
    private Socket clientSocket;
    private Client client;
    public CheckNewUser(){};
    public CheckNewUser(Client client,Socket clientSocket){
        this.client = client;
        this.clientSocket = clientSocket;
    }
    @Override
    public Boolean call(){
        try {
            BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter output = new PrintWriter(clientSocket.getOutputStream(),true);
           
            String response,username;
            do{
                username = client.getTxtUsername().getText();
                output.println(username);
                response = input.readLine();
                if("invalid".equals(response)){
                    System.out.println("Username already taken!");
                    client.getTxtUsername().setText("");
                    client.getCardLayout().show(client.getPnlBg(), "pnlSignin");
                    
                    return false;
                }
            }while("invalid".equals(response));
            response = "";
            /*
                I have created a new thread to add users in pnlShowActiveUsers  ..
                bcoz if there is a infinite loop and cardLayout().show() in the same thread then show() 
                never shows the UI.
            */
//            thread.start();
            new AddUsers(client, clientSocket).start();
            return true;
            
        }catch(IOException e){}
        return true;
    }
    
}
