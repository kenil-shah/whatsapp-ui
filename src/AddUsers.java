
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenil
 * Client Side Thread
 */
public class AddUsers extends Thread{
    private Client client;
    private Socket clientSocket;
    private String username;
    private boolean wasAbleToConnect = true;
    private List<String> usernameList = new ArrayList<>();
    
   
    public AddUsers(){};
    public AddUsers(Client client, Socket clientSocket){
        this.client = client;
        this.clientSocket = clientSocket;
    }
    @Override
    public void run(){
        String response;
        try(Socket configClientSocket = new Socket("localhost",2002)){
            BufferedReader configInput = new BufferedReader(new InputStreamReader(configClientSocket.getInputStream()));
            PrintWriter configOutput = new PrintWriter(configClientSocket.getOutputStream(), true);
//            String userExited = "((.*)exited$)";
            String userExited = "(([^\\s]*)(\\s)*(exited$))";
            Pattern pattern = Pattern.compile(userExited);
            int rowCount = 8, userCount = 0;
            String dummyUsername = "";
            while(true){
                response = configInput.readLine();
                if(response != null){
                    Matcher matcher = pattern.matcher(response);
                    if(response.contains("exited")){
//                        System.out.println("Hello i came here in if");
                        while(matcher.find()){
                            System.out.println("Username exited : "+ matcher.group(2));
                            this.username = matcher.group(2);
                            
                            if(!dummyUsername.equals(this.username)){
                                usernameList.remove(this.username);
                                Thread.sleep(250);
                                client.removeUser(this.username);
                                dummyUsername = this.username;
                            }
                        }
                        response = "";
                    }else if(!usernameList.contains(response)){
//                        System.out.println("Hello i came here in else : " + response);
                        usernameList.add(response);
                        userCount++;
                        if(userCount > rowCount){
                            rowCount = userCount;
                        }
                        if(!response.equals(client.getTxtUsername().getText())){
//                            Thread.sleep(500);
                            client.addNewUser(rowCount, response);
                            
                        }   
                    }
                }
            }           
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Username in catch : " + this.username);
//            usernameList.remove(this.username);
//            client.removeUser(this.username);
        }finally {
            System.out.println("AddUsers: TATA BYE BYE KHATAM");
//            try {
////                clientSocket.close();
//            }catch(IOException e){
//                e.printStackTrace();
//            } 
            
        }      
    }
    
    private void displayUsernameList(){
        for(String username : usernameList){
            System.out.println("Username : " + username);
        }
    }
}
