
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenil
 * Client Side Thread
 */
public class ReadUserMessages extends Thread{
    private Socket socket;
    private Client client;
    private JTextField txtMessage;
    public Boolean messageComplete = false;
    private String clientUsername;
    
    public ReadUserMessages(Socket socket, Client client){
        this.socket = socket;
        this.client = client;
        clientUsername = client.getUsernameToBeMessaged();
        this.txtMessage = client.getTxtMessage(clientUsername);
    }
//    public static void setNewRowCount(int newCount){
//        newRowCount = newCount;
//    }
    public void run(){
        String message, jsonMessage;
        int rowCount = 8;
        int newRowCount = 0;
        try {
            PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//            File f = new File("chats/"+client.getUsername()+".json");
//            PrintStream writer;
            System.out.println("User Messages are being read!!");
            while(true){
                do {
                     message = client.getTxtMessage(clientUsername).getText();
                 }while(!messageComplete );
                 messageComplete = false;
                 txtMessage.setText("");
                 String usernameToBeMessaged = client.getUsernameToBeMessaged();
                 String username = client.getUsername();
                 jsonMessage = JSON.createJSONMessage(username,usernameToBeMessaged , message);
                 
                 showMessagesOnUI(usernameToBeMessaged, message);
                 
                  output.println(jsonMessage);
                                 
            }
                
                
        }catch(IOException e){
            e.printStackTrace();
        }
        
        
    }
    private void showMessagesOnUI(String usernameToBeMessaged, String message){
        GridBagConstraints c = new GridBagConstraints();
        JPanel pnlShowMessages = client.getPnlShowMessages(usernameToBeMessaged);
        JLabel l = new JLabel(message, JLabel.RIGHT);
        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        p.add(l);
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.gridheight = 1;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.NORTHEAST;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.ipadx = 20;
        p.setBorder(new EmptyBorder(5,20,5,8));
        c.insets = new Insets(5,0,5,0);
        pnlShowMessages.add(p, c);
        pnlShowMessages.updateUI();
        c.weightx = 0.0;
        c.weighty = 0.0;         
    }
}
