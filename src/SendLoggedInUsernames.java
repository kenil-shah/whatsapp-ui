
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenil
 */
public class SendLoggedInUsernames extends Thread{
    private Socket configClientSocket;
    private ConcurrentHashMap<String , Integer> users = Server.getUsers();
    private List<Socket> configSocketList = Server.getConfigSocketList();
    private PrintWriter configOutput;
    private ReentrantLock bufferLock;
    private boolean isAlive = true;
    public SendLoggedInUsernames(Socket configClientSocket, ReentrantLock bufferLock){
        this.configClientSocket = configClientSocket;
        this.bufferLock = bufferLock;
    }
    public void setIsAlive(boolean isAlive){
       this.isAlive = isAlive; 
    } 
           
    @Override
    public void run(){
        try {
            configOutput = new PrintWriter(configClientSocket.getOutputStream(),true);
            while(isAlive){
                sendLoggedInUsernames();
            }
        }catch(IOException e){
//            System.out.println("This in catch : " + this);
//            e.printStackTrace();
        }finally{
              
//            System.out.println("SendLoggedInUsernames tata bye bye khatam");
        }
        
    }
    
    private void sendLoggedInUsernames() throws IOException{
        while(true){
           if(!Server.getUsers().isEmpty()){
                for(String username : Server.getUsers().keySet()){
                    int val = Server.getUsers().get(username);
                    if(val == 0){
//                            System.out.println("Hello " + Server.getUsers().get(username));
                        bufferLock.lock();
                        Server.removeUser(username);
                        bufferLock.unlock();
//                        System.out.println("Exited : " + username);
                        for(Socket configSocket : Server.getConfigSocketList()){
                            new PrintWriter(configSocket.getOutputStream(),true).println(username+" exited");
                        }
                    }else{
                        configOutput.println(username); 
                    }
                }
                break;
            } 
        }
    }
}
