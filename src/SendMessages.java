import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.locks.ReentrantLock;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenil
 */
public class SendMessages extends Thread{
    private Socket socket;
    private Socket outputSocket;
    private String username = "";
    private ReentrantLock bufferLock;
    public SendMessages(){};
    public SendMessages(Socket socket, String username, ReentrantLock bufferLock){
        this.socket  = socket;
        this.username = username;
        this.bufferLock = bufferLock;
    }
    
    @Override
    public void run(){
        try {
            BufferedReader input = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            PrintWriter output;
            String response, finalMessage = "", message,usernameToBeMessaged;
            while(true){
                do{
                    response = input.readLine();
                    if(response != null ){
                        finalMessage += response + "\n";
                    }
                }while(!"};null".contains(response));
                message = JSON.getMessageFromJSON(finalMessage);
                if(message != null ){
                    usernameToBeMessaged = JSON.getUsernameToBeMessagedFromJSON(finalMessage);
                    this.outputSocket = Server.getSocket(usernameToBeMessaged);
                    output = new PrintWriter(this.outputSocket.getOutputStream(), true);
                    output.println(finalMessage);
//                    System.out.println("Message Sent!");
                    finalMessage = "";
                }
            }
        }catch(Exception e){
//            e.printStackTrace();
        }finally {
            System.out.println(this.username + " logged out");
//                Server.removeUser(this.username);
            bufferLock.lock();
            try {
                Server.setSocketToNull(this.username);
            } finally {
                bufferLock.unlock();
            }
           
        }
    }
}
