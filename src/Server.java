
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenil
 */
public class Server {
    // Using them in a threaded environment throws ConcurrentModificationException sometimes
//    private static HashMap<String, Integer> users = new HashMap<>();
//    private static List<Socket> socketList = new ArrayList<>();   
//    private static List<Socket> configSocketList = new ArrayList<>();
    private static ConcurrentHashMap<String, Integer> users = new ConcurrentHashMap<>();
    private static List<Socket> socketList = new ArrayList<>();   
    private static List<Socket> configSocketList = new ArrayList<>();
    private static ReentrantLock bufferLock;
    private static List<String> exitedUsers = new ArrayList<>();
    private static ConcurrentHashMap<Socket, SendLoggedInUsernames> sendLoggedInUsernamesMap = new ConcurrentHashMap<>();
    public static void displayMap(){
    	if(users.isEmpty()){
            System.out.println("There are no users logged in");
            return;
    	}
    	System.out.println("HashMap : ");
    	for(String key : users.keySet()){
            System.out.println(key + " : " + users.get(key));
    	}
    }
    public static void displaySocketList(){
    	if(socketList.isEmpty()){
    		System.out.println("The Socket list is empty!");
    		return;
    	}
//    	System.out.println("Socket List : ");
    	for(Socket socket : socketList){
    		System.out.println("Socket : " + socket);
    	}
    }
    public static int getUserCount(){
//        displayMap();
        return users.size();
    }
    public static ConcurrentHashMap<String,Integer> getUsers(){
        return users;
    }
    public static List<Socket> getSocketList(){
        return new ArrayList<Socket>(socketList);
    }
    public static boolean addUser(String username, Socket socket){
        if(!users.containsKey(username)){
            if(exitedUsers.contains(username)){
                System.out.println("Welcome back : " + username);
                exitedUsers.remove(username);
            }
            users.put(username, socket.getPort());
            socketList.add(socket);
//            displayMap();
            return true;
        }
        return false;
    }
    private static Integer getSocketNumber(String username){
        Integer socketNumber = users.get(username);
        return socketNumber;
    }
    public static Socket getSocket(String username){
        for(Socket socket : socketList){ 
            if((int)getSocketNumber(username) == 0){
                return socket;
            }else if(socket.getPort() == (int)getSocketNumber(username)){
                return socket;
            } 
        }
        return null;
    }
    public static void setSocketToNull(String username){
        users.replace(username, 0);
    }
    public static boolean removeUser(String username){
        bufferLock.lock();
        try {
//        System.out.println(users.remove(username));
            if (!exitedUsers.contains(username)) {
                Thread.sleep(100);
//            System.out.println("Before");
                Socket clientSocket = getSocket(username);
                exitedUsers.add(username);
                sendLoggedInUsernamesMap.get(clientSocket).setIsAlive(false);
                Thread.sleep(50);
                sendLoggedInUsernamesMap.remove(clientSocket);
                socketList.remove(clientSocket);
                users.remove(username);
                displayMap();
                bufferLock.unlock();
                return true;
            }
            
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        bufferLock.unlock();
        return false;
        
    }
    public static List<Socket> getConfigSocketList(){
        return new ArrayList<Socket>(configSocketList);
    }
    public static boolean addConfigSocket(Socket socket){
        if(!configSocketList.contains(socket)){
            configSocketList.add(socket);
            return true;
        }
        return false;
    }
    public static boolean removeConfigSocket(Socket socket){
        if(configSocketList.contains(socket)){
            configSocketList.remove(socket);
            return true;
        }
        return false;
    }
    public static void main(String[] args){
        try(ServerSocket serverSocket = new ServerSocket(1106)){
            while(true){
                Socket clientSocket = serverSocket.accept();
                bufferLock = new ReentrantLock();
                new RegisterUser(clientSocket, bufferLock).start();
                new SendMessages();
                try(ServerSocket configServerSocket = new ServerSocket(2002)){
                    Socket configClientSocket = configServerSocket.accept();
                    SendLoggedInUsernames s = new SendLoggedInUsernames(configClientSocket, bufferLock);
                    sendLoggedInUsernamesMap.put(clientSocket, s);
                    s.start();
                    Server.addConfigSocket(configClientSocket);
                    
                }catch(IOException e){
//                    e.printStackTrace();
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Cannot connect to server : " + e.getMessage());
        }
    }   
}
